export type OptionValueName = string
export type OptionName = string

type InvalidatedOptionValueName = string

export type Selections = {
    [optionName: string]: OptionValueName[]
}

export type Invalidates = {
    [invalidatedOptionName: string]: InvalidatedOptionValueName[]
}

type OptionValues = {
    [optionValueName: string]: {
        id: OptionValueName,
        invalidates: Invalidates
    }
}

export type GlobalOptions = {
    [optionName: string]: OptionValues
}

export type ProductOptions = {
    [optionName: string]: OptionValueName[]
}