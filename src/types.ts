export type ProductType = {
    id: string,
    title: string
}

export type ProductCategory = {
    _id: string,
    name: string
}

export type ProductDescriptionType = {
    productId: string,
    productType: string,
    printType: string
}