import * as R from 'ramda'

export type OptionValueName = string
export type OptionName = string

type InvalidatedOptionValueName = string

export type Selections = {
    [optionName: string]: OptionValueName[]
}

export type Invalidates = {
    [invalidatedOptionName: string]: InvalidatedOptionValueName[]
}

type OptionValues = {
    [optionValueName: string]: {
        id: OptionValueName,
        invalidates: Invalidates
    }
}

export type GlobalOptions = {
    [optionName: string]: OptionValues
}

export type ProductOptions = {
    [optionName: string]: OptionValueName[]
}

/**
 * Given an optionValueName belonging to an option, get the invalidate rules define
 * in the options object.
 * @param optionName 
 * @param optionValueName 
 * @param options 
 */
const getInvalidatesOfOptionValue = (optionName: OptionName,
    optionValueName: OptionValueName, globalOptions: GlobalOptions): Invalidates => {

    const option = globalOptions[optionName]

    if (!option) {
        console.error(`Message From Programmer: globalOptions does not have option: ${optionName}`)
        return {}
    }

    if (!option[optionValueName]) {
        console.warn(`Message From Programmer:
        ${optionName} does not have value ${optionValueName}`)
        return {}
    }

    //const invalidates = option[optionValueName].invalidates
    const invalidates = option[optionValueName].invalidates

    if (!invalidates) {
        console.error('Option value does not have invalidates')
        return {}
    }

    return invalidates
}

/**
 * A normal merge causes the array of option value names to be override by
 * the last object. Instead we want to concat the arrays.
 * @param allInvalidates 
 */
const mergeInvalidates = (allInvalidates: Invalidates[]): Invalidates => {
    return R.reduce((acc: Invalidates, elem) => {
        // Merge with goes through every key and then we concat the arrays.
        return R.mergeWith((valueA, valueB) => {
            return R.concat(valueA, valueB)
        }, acc, elem)
    }, {}, allInvalidates)
}

export const mergeSelectionInvalidates = (selections: Selections, globalOptions: GlobalOptions): Invalidates => {
    const allSelectionsInvalidates = R.mapObjIndexed((selectedOptionValues: OptionValueName[], selectedOption: OptionName) => {
        // Map over the list of selected option values and return their Invalidates.
        const allInvalidates = R.map((selectedOptionValue) => {
            return getInvalidatesOfOptionValue(selectedOption, selectedOptionValue, globalOptions)
        }, selectedOptionValues)

        // Merge each selected option value's Invalidates.
        const mergedInvalidates: Invalidates = mergeInvalidates(allInvalidates)
        // R.mapAccum((accum, invalidates: Invalidates) => {
        //     return R.mergeDeepLeft(accum, invalidates)
        // }, allInvalidates)
        return mergedInvalidates
    }, selections)

    // Merge the invalidates of each selection.
    const merged: Invalidates = mergeInvalidates(R.values(allSelectionsInvalidates))

    return merged
}

export const getNewProductOptionsFromSelections = (selections: Selections, currentProductOptions: ProductOptions,
    globalOptions: GlobalOptions): ProductOptions => {
    // Merge the invalidates of each selection.
    const invalidates: Invalidates = mergeSelectionInvalidates(selections, globalOptions)

    // Return the current product options, without the merged invalidates.
    const newProductOptions: ProductOptions = R.mapObjIndexed((optionValues: OptionValueName[],
        optionName: OptionName) => {
        const invalidatedOptionValues: OptionValueName[] = invalidates[optionName]
        return R.without(invalidatedOptionValues, optionValues)
    }, currentProductOptions)
    return newProductOptions
}

console.log('findme');