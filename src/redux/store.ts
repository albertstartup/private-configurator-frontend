import { createStore } from 'redux'
import AppReducer from './reducers'
import { makeVariablesGlobal } from '../react/lib/lib';

const store = createStore(AppReducer);

makeVariablesGlobal({store})

export default store