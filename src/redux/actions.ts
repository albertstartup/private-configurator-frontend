import { createAction } from 'typesafe-actions';
import { ProductType, ProductDescriptionType, ProductCategory } from '../types';

export const setProducts = createAction('products/SET', (resolve) => {
    return (products: ProductType[]) => resolve(products)
})

export const setProductDescriptions = createAction('productDescriptions/SET', (resolve) => {
    return (productDescriptions: ProductDescriptionType[]) => resolve(productDescriptions)
})

export const setProductCategories = createAction('productCategories/SET', (resolve) => {
    return (productCategories: ProductCategory[]) => resolve(productCategories)
})