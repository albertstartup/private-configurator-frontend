import { ActionType, getType } from 'typesafe-actions'

import * as actions from './actions'
import { ProductType, ProductDescriptionType, ProductCategory } from '../types';

export type AppActions = ActionType<typeof actions>

export type StateType = {
    products: ProductType[],
    productDescriptions: ProductDescriptionType[],
    productCategories: ProductCategory[]   
}

const initialState = {
    products: [],
    productDescriptions: [],
    productCategories: []
}

export default (state:StateType = initialState, action: AppActions) => {
    switch(action.type) {
        case getType(actions.setProducts):
            return {...state, products: action.payload}
        case getType(actions.setProductDescriptions):
            return {...state, productDescriptions: action.payload}
        case getType(actions.setProductCategories):
            return {...state, productCategories: action.payload}
        default:
            return state
    }
}