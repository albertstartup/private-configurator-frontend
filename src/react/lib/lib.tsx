import { Divider } from 'antd'
import axios from 'axios'
import {map} from 'ramda'
import { ProductType, ProductDescriptionType, ProductCategory } from '../../types'
import * as R from 'ramda'
import {validate} from 'jsonschema'
import { object } from 'prop-types';

declare let API_URL:string

declare let process: {browser: boolean}

// TODO: Make this a package.
/**
 * This uses ES2015 shorthand proprety names to make variables global.
 * @param objectWithVariables An object of variables to make global. Use shorthand proprety notation ({variableName}).
 */
export const makeVariablesGlobal = (objectWithVariables: {}) => {
    if (process.browser) {
        const currentPrivateVariables = (window as any).p
        const newPrivateVariables = {...currentPrivateVariables, ...objectWithVariables};
        (window as any).p = newPrivateVariables
    }
}

export const CustomDivider = ({ text }: { text: string }) => {
    return <Divider style={{ width: '100%' }}>
        {text}
    </Divider>
}

export const getProducts = async () => {
    try {        
        return (await axios.get(`${API_URL}/api/products`)).data
    } catch (error) {
        throw error
    }
}

export const getProductDescription = async (productId: string) => {
    try {        
        return (await axios.get(`${API_URL}/api/getProductDescription/${productId}`)).data
    } catch (error) {
        throw error
    }
}

export const getProductDescriptionsAndId = async (productIds: string[]) => {
    return await Promise.all(map(async (productId: string) => {
        const productDescription = await getProductDescription(productId)
        return {productId, ...productDescription}
    }, productIds))
}

export const createProductCategory = async (categoryName: string) => {
    try {
        return (await axios.post(`${API_URL}/api/createProductCategory/${categoryName}`)).data
    } catch (error) {
        throw error
    }
}

export const fetchProductCategories = async (): Promise<ProductCategory[]> => {
    try {        
        const response = await axios.get(`${API_URL}/api/getProductCategories`)
        //console.log();        
        const t = validate(response.data, {"type": "array", "items": {"type": "object"}})
        makeVariablesGlobal({t})
        return response.data
    } catch (error) {
        throw error
    }
}

/**
 * If currentProducts is empty, call the api and store the results in the redux store.
 * @param currentProducts 
 * @param setProducts 
 * @param setProductDescriptions 
 */
export const setProductsAndProductDescriptions = async (
    currentProducts: ProductType[],
    setProducts: (x: ProductType[]) => void,
    setProductDescriptions: (x: ProductDescriptionType[]) => void) => {
    if (R.isEmpty(currentProducts)) {
        console.log('products is empty');

        const getId = R.pluck('id')

        // Todo: await blocks the next await, unless you await the reference.
        const products = await getProducts()

        setProducts(products)

        const productIds: string[] = getId(products)

        const productDescriptions = await getProductDescriptionsAndId(productIds)

        setProductDescriptions(productDescriptions)
    }
}

export const setProductCategoriesInStore = async (currentProductCategories: ProductCategory[], 
    setProductCategories: (x: ProductCategory[]) => void) => {
    if (R.isEmpty(currentProductCategories)) {
        console.log('product categories is empty')
        
        const categories = await fetchProductCategories()

        setProductCategories(categories)
    }
}

if(process.browser) {
    (window as any).p_getProducts = getProducts;
    (window as any).p_getProductDescription = getProductDescription
}