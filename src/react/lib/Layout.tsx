import { useEffect, FunctionComponent } from "react";
import { pluck } from "ramda";
import { getProducts, getProductDescriptionsAndId } from "./lib";
import { ProductDescriptionType, ProductType } from "../../types";
import * as R from 'ramda'
import { Layout as AntLayout, Menu, Divider } from 'antd'
import 'antd/dist/antd.css'
import '../../styles/styles.css'
import Link from "next/link";
import { ClipLoader } from "react-spinners";

type LayoutProps = {
    headerText: string
}

type SpinnerProps = {
    spinnerActive: boolean
}

export const Spinner: FunctionComponent<SpinnerProps> = (props) => {
    // Parent must be postion: relative so that absolute is based on that parent.
    // Padding is to accomadate the padding on the parent.
    return < div style={{
        position: 'absolute', paddingRight: '6em',
        textAlign: 'center', width: '100%'
    }}>
        <ClipLoader/>
    </div >
}

/**
 * MyLayout is passed products and descriptions by redux connect. MyLayout then passes thoose to the render prop.
 * @param props
 */
const Layout: FunctionComponent<LayoutProps> = (props) => {
    const { headerText } = props

    return <>
        <AntLayout hasSider={true} style={{ height: '100%' }}>
            <AntLayout.Sider>
                <Menu
                    theme="dark"
                    mode="vertical"
                    defaultSelectedKeys={['2']}
                    style={{ marginTop: '64px', lineHeight: '64px' }}
                >
                    <Menu.Item key="1">Create Product</Menu.Item>
                    <Menu.Item key="2">Edit Product</Menu.Item>
                </Menu>
            </AntLayout.Sider>
            <AntLayout.Header style={{ padding: '0em', position: 'fixed', zIndex: 1, width: '100%' }}>
                <Menu
                    theme="dark"
                    mode="horizontal"
                    defaultSelectedKeys={['2']}
                    style={{ lineHeight: '64px' }}
                >
                    <Menu.Item key="1">Indie Printing</Menu.Item>
                    <Menu.Item key="2"><Link href={'/'}><a>Home</a></Link></Menu.Item>
                    <Menu.Item key="3"><Link href={'/chooseProduct'}><a>Manage Products</a></Link></Menu.Item>
                    <Menu.Item key="4"><Link href={'/createOption'}><a>Manage Options</a></Link></Menu.Item>
                    <Menu.Item key="5"><Link href={'/createCategory'}><a>Manage Categories</a></Link></Menu.Item>
                </Menu>
            </AntLayout.Header>
            <AntLayout.Content>
                <div style={{
                    position: 'relative',
                    height: '100%', margin: '7em 3em 3em 3em', backgroundColor: 'white'
                }}>
                    <div style={{ padding: '3em 6em 3em 6em', marginBottom: '3em', backgroundColor: 'white' }}>
                        <div style={{ display: 'flex', justifyContent: 'center', paddingBottom: '1.5em' }}>
                            <div style={{ textAlign: 'center', width: '30em' }}>
                                <h1>{headerText}</h1>
                                <Divider />
                            </div>
                        </div>
                        <Spinner/>
                        {props.children}
                    </div>
                </div>
            </AntLayout.Content>
        </AntLayout>
    </>
}

export default Layout