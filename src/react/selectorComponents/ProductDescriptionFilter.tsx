import { CustomDivider } from '../lib/lib'
import { FunctionComponent } from 'react';
import ProductPropertySelector from './ProductPropertySelector';
import { ProductType, ProductCategory } from '../../types';
import * as R from 'ramda'

type ProductDescriptionFilterProps = {
    productTypeDividerText: string,
    printTypeDividerText: string,
    productCategoryDivideText?: string,
    onSelectProductType?: (productType: string) => void,
    onSelectPrintType?: (productType: string) => void,
    onSelectProductCategory?: (productType: string) => void,
    productCategories?: ProductCategory[],
    productTypes?: string[],
    printTypes?: string[]
}

const ProductDescriptionFilter: FunctionComponent<ProductDescriptionFilterProps> = (props) => {
    // This may be an anti pattern, it may be better to just use props.
    const { onSelectProductType, onSelectPrintType, onSelectProductCategory,
        productTypeDividerText, printTypeDividerText, productCategoryDivideText } = props
        
    return <>
        <CustomDivider text={productTypeDividerText} />

        <div style={{ marginBottom: '3em' }}>
            <ProductPropertySelector onSelect={onSelectProductType}
            productProperties={['Flat', 'Book', 'Large Format']} />
        </div>

        <CustomDivider text={printTypeDividerText} />

        <div style={{ marginBottom: '3em' }}>
            <ProductPropertySelector onSelect={onSelectPrintType}
            productProperties={['Digital', 'Offset']} />
        </div>

        {productCategoryDivideText && props.productCategories &&
            <>
                <CustomDivider text={productCategoryDivideText} />

                <div style={{ marginBottom: '3em' }}>
                    <ProductPropertySelector onSelect={onSelectProductCategory}
                    productProperties={R.pluck('name', props.productCategories)} />
                </div>
            </>
        }
    </>
}

export default ProductDescriptionFilter