import { equals, always, where, pluck, filter, includes } from 'ramda'
import { ProductType, ProductDescriptionType } from '../../types';

export type ProductSelectorFilterObj = {
    productCategory?: string,
    printType?: string,
    productType?: string
}

const filterProductsWithDescriptions = (products: ProductType[], productDescriptions: ProductDescriptionType[], filterObj: ProductSelectorFilterObj) => {
    const spec = {
        productCategory: filterObj.productCategory ? equals(filterObj.productCategory) : always(true),
        printType: filterObj.printType ? equals(filterObj.printType) : always(true),
        productType: filterObj.productType ? equals(filterObj.productType) : always(true)
    }    
    const filteredproductDescriptions: ProductDescriptionType[] = filter(where(spec), productDescriptions)
    const filteredIds = pluck("productId", filteredproductDescriptions)
    const filteredProducts = filter((product) => includes(product.id, filteredIds),
        products)
    return filteredProducts
}

export default filterProductsWithDescriptions