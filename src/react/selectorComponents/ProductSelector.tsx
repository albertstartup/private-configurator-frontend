import { FunctionComponent } from "react";
import { CustomDivider} from '../lib/lib'
import { map } from "ramda";
import { Button } from "antd";
import filterProductsWithDescriptions, { ProductSelectorFilterObj } from "./filterProductsWithDescriptions"
import { ProductType, ProductDescriptionType } from "../../types";

type ProductSelectorProps = {
    dividerText: string,
    filterObj: ProductSelectorFilterObj,
    products: ProductType[],
    productDescriptions: ProductDescriptionType[],
}

const ProductSelector: FunctionComponent<ProductSelectorProps> = ({ dividerText, filterObj, products, productDescriptions }) => {

    const filteredProducts =  filterProductsWithDescriptions(products,
        productDescriptions, filterObj)

    return <>
        <CustomDivider text={dividerText} />
        {
            map((product) => {
                return <Button style={{ marginRight: '1em' }} type='dashed'>{product.title}</Button>
            }, filteredProducts)
        }
    </>
}

export default ProductSelector