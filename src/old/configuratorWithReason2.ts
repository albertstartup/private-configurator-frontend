// TODO: There is an implicit order to which the options get selected, so you dont need the reason feature

import * as R from 'ramda'

export type OptionValueName = string
export type OptionName = string

type InvalidatedOptionValueName = string

type Selections = {
    [optionName: string]: OptionValueName[]
}

export type Invalidates = {
    [invalidatedOptionName: string]: InvalidatedOptionValueName[]
}

type OptionValues = {
    [optionValueName: string]: {
        id: OptionValueName,
        invalidates: Invalidates
    }
}

type GlobalOptions = {
    [optionName: string]: OptionValues
}

type ProductOptions = {
    [optionName: string]: OptionValueName[]
}

type InvalidatesWithReason = {
    [optionName: string]: { optionValueName: OptionValueName, reason: string }[]
}

/**
 * Given an optionValueName belonging to an option, get the invalidate rules define
 * in the options object.
 * @param optionName 
 * @param optionValueName 
 * @param options 
 */
const getInvalidatesOfOptionValue = (optionName: OptionName,
    optionValueName: OptionValueName, globalOptions: GlobalOptions): Invalidates => {

    const option = globalOptions[optionName]

    if (!option) {
        console.error(`Message From Programmer: globalOptions does not have option: ${optionName}`)
        return {}
    }

    if (!option[optionValueName]) {
        console.warn(`Message From Programmer:
        ${optionName} does not have value ${optionValueName}`)
        return {}
    }

    const invalidates = option[optionName].invalidates

    if (!invalidates) {
        console.error('Option value does not have invalidates')
        return {}
    }

    return invalidates
}

/**
 * An option may have multiple optionvalues selected, in which case we have to merge
 * their invalidates.
 * @param optionName 
 * @param optionValueNames 
 * @param options 
 */
// const mergeInvalidates = (optionName: OptionName, selectedOptionValues: OptionValueName[],
//     options: Options): Invalidates => {
//     const allInvalidates = R.map((selectedOptionValueName) => {
//         const invalidates = getInvalidatesOfOptionValue(optionName, selectedOptionValueName,
//             options)

//         const invalidatesWithReason: InvalidatesWithReason = R.mapObjIndexed((
//             invalidatedOptionValueNames: InvalidatedOptionValueName[],
//             invalidatedOptionName: string) => {
//             return R.map((invalidatedOptionValueName) => {
//                 return { optionValueName: invalidatedOptionValueName, reason: optionName + ',' + selectedOptionValueName }
//             }, invalidatedOptionValueNames)
//         }, invalidates)
//         return invalidates
//     }, selectedOptionValues)
//     return R.mergeAll(allInvalidates)
// }

export const mergeSelectionInvalidates = (selections: Selections, globalOptions: GlobalOptions): Invalidates => {


    const allSelectionsInvalidates = R.mapObjIndexed((selectedOptionValues: OptionValueName[], selectedOption: OptionName) => {

        // Map over the list of selected option values and return their Invalidates.
        const allInvalidates = R.map((selectedOptionValue) => {
            return getInvalidatesOfOptionValue(selectedOption, selectedOptionValue, globalOptions)
        }, selectedOptionValues)

        // Merge each selected option value's Invalidates.
        const mergedInvalidates: Invalidates = R.mergeAll(allInvalidates)
        return mergedInvalidates

    }, selections)

    const merged: Invalidates = R.mergeAll(R.values(allSelectionsInvalidates))

    return merged
}

export const getNewProductOptionsFromSelections = (selections: Selections, productOptions: ProductOptions,
    globalOptions: GlobalOptions): ProductOptions => {
    const invalidates: Invalidates = mergeSelectionInvalidates(selections, globalOptions)
    const newProductOptions: ProductOptions = R.mapObjIndexed((optionValues: OptionValueName[],
        optionName: OptionName) => {
            const invalidatedOptionValues: OptionValueName[] = invalidates[optionName]
            return R.without(invalidatedOptionValues, optionValues)
    }, productOptions)
    return newProductOptions
}

/**
 * This returns an InvalidatesWithReason from an Invalidates. An invalidates
 * looks like this: { 'color': ['blue', 'red'] }.
 * We want this: { 'color': [ {optionValueName: 'blue', reason: 'reason'} ] }
 * @param selectedOptionValue 
 * @param selectedOption 
 * @param invalidates 
 */
export const createInvalidatesWithReasonFromSelectedOptionValue = (selectedOptionValue: OptionValueName,
    selectedOption: OptionName, invalidates: Invalidates): InvalidatesWithReason => {
    /**
     * A selected option value, may invalidate multiple option values from different options.
     */
    return R.mapObjIndexed((invalidatedOptionValueNames) => {
        /**
         * An invalidated option can have many invalidated option values: 'color': ['red', 'blue'].
         * We want to turn that array into an array of objects: 'color': [{name, reason}, {name, reason}]
         */
        return R.map((invalidatedOptionValueName) => {
            return { optionValueName: invalidatedOptionValueName, reason: selectedOption + ',' + selectedOptionValue }
        }, invalidatedOptionValueNames)
    }, invalidates)
}

/**
 * A Selection looks like this: { color: ['blue', 'red'], size: ['small', 'medium'] }.
 * We want to map through the object and collect the Invalidates of each selected option value.
 * @param selections
 * @param options
 */
// const createInvalidatesWithReasonFromSelections = (selections: Selections,
//     options: Options): {[optionName: string]: InvalidatesWithReason[]} => {
//     return R.mapObjIndexed((selectedOptionValues: OptionValueName[], optionName: OptionName) => {
//         //const mergedInvalidates: Invalidates = mergeInvalidates(optionName,
//         //selectedOptionValues, options)

//         return R.map((selectedOptionValue)  => {
//             const invalidates = getInvalidatesOfOptionValue(optionName, selectedOptionValue, options)
//             const invalidatesWithReason =
//                 createInvalidatesWithReasonFromSelectedOptionValue(selectedOptionValue, optionName, invalidates)
//             return invalidatesWithReason
//         }, selectedOptionValues)
//     }, selections)
// }

// const createInvalidatesWithReasonFromSelection = (selections: Selection,
//     options: Options): InvalidatesWithReason => {
//     R.mapObjIndexed((selectedOptionValues: OptionValueName[], optionName: OptionName) => {
//         const mergedInvalidates: Invalidates = mergeInvalidates(optionName,
//             selectedOptionValues, options)

//         return []
//     }, selections)
// }