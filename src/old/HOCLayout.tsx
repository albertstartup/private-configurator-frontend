// HOC implementation of Layout. Not able to use hooks, so I will use render props.

import Header from "./Header";
import { useState, useEffect, FunctionComponent, ComponentType, Component } from "react";
import { pluck } from "ramda";
import { getProducts, getProductDescriptionsAndId } from "./lib";
import { ProductDescriptionType, ProductType } from "../../types";

type LayoutProps = {
    initialProducts?: ProductType[],
    initialProductDescriptions?: ProductDescriptionType[],
    WrappedComponent: ComponentType<any>
}

const withLayout = (WrappedComponent: ComponentType<any>): ComponentType<any> => {
    // const [products, setProducts] = useState([])
    // const [productDescriptions, setProductDescriptions] = useState([])    

    // useEffect(() => {
    //     (async () => {
    //         const getId = pluck('id')

    //         const products = await getProducts()

    //         setProducts(products)

    //         const productIds: string[] = getId(products)

    //         const productDescriptions = await getProductDescriptionsAndId(productIds)

    //         console.log('productDescriptions :', productDescriptions)

    //         setProductDescriptions(productDescriptions)
    //     })()
    // }, [false])

    return () => {
        return <>
            <Header />
            <div style={{ marginTop: '8em', marginLeft: '10em', width: '60em', }}>

                <WrappedComponent products={[]} productDescriptions={[]} />

            </div>
        </>        
    }
}

export default withLayout