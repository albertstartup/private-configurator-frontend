import { useEffect, FunctionComponent, Dispatch } from "react";
import { pluck } from "ramda";
import { getProducts, getProductDescriptionsAndId } from "./lib/lib";
import { ProductDescriptionType, ProductType } from "../types";
import { connect } from 'react-redux'
import { StateType, AppActions } from "../redux/reducers";
import * as actions from '../redux/actions'
import * as R from 'ramda'
import { Layout, Card, Menu } from 'antd'
import '../styles/styles.css'

type RenderComponentProps = {
    products: ProductType[],
    productDescriptions: ProductDescriptionType[]
}

type LayoutProps = {
    render: FunctionComponent<RenderComponentProps>
    products: ProductType[],
    productDescriptions: ProductDescriptionType[],
    setProducts: (products: ProductType[]) => any,
    setProductDescriptions: (productDescriptions: ProductDescriptionType[]) => any
}

/**
 * If currentProducts is empty, call the api and store the results in the redux store.
 * @param currentProducts 
 * @param setProducts 
 * @param setProductDescriptions 
 */
const setProductsAndProductDescriptions = async (currentProducts: ProductType[],
    setProducts: (x: ProductType[]) => void, setProductDescriptions: (x: ProductDescriptionType[]) => void) => {
    if (R.isEmpty(currentProducts)) {
        console.log('products is empty');

        const getId = pluck('id')

        const products = await getProducts()

        setProducts(products)

        const productIds: string[] = getId(products)

        const productDescriptions = await getProductDescriptionsAndId(productIds)

        setProductDescriptions(productDescriptions)
    }
}

/**
 * MyLayout is passed products and descriptions by redux connect. MyLayout then passes thoose to the render prop.
 * @param props
 */
const MyLayout: FunctionComponent<LayoutProps> = (props) => {
    const { products = [], productDescriptions = [],
        render, setProducts, setProductDescriptions } = props;

    useEffect(() => {
        (async () => {
            
        })()
    }, [false])

    return <>
        <Layout hasSider={true} style={{ height: '100%' }}>
            <Layout.Sider></Layout.Sider>
            <Layout.Header style={{ padding: '0em', position: 'fixed', zIndex: 1, width: '100%' }}>                
                <Menu
                    theme="dark"
                    mode="horizontal"
                    defaultSelectedKeys={['2']}
                    style={{ lineHeight: '64px' }}
                >
                    <Menu.Item key="1">Indie Printing</Menu.Item>
                    <Menu.Item key="2">Manage Products</Menu.Item>
                    <Menu.Item key="3">Manage Options</Menu.Item>
                </Menu>
                Indie Printing
            </Layout.Header>
            <Layout.Content style={{ display: 'flex', margin: '2em', justifyContent: 'center' }}>
                <Card>
                    <div style={{ marginTop: '8em', marginLeft: '10em', width: '60em', }}>
                        {render({ products: products, productDescriptions: productDescriptions })}
                    </div>
                </Card>
            </Layout.Content>
        </Layout>        
    </>
}

const mapStateToProps = (state: StateType) => {
    return {
        products: state.products,
        productDescriptions: state.productDescriptions
    }
}

const mapDispatchToProps = (dispatch: Dispatch<AppActions>) => {
    return {
        setProducts: (products: ProductType[]) => dispatch(actions.setProducts(products)),
        setProductDescriptions: (productDescriptions: ProductDescriptionType[]) => dispatch(actions.setProductDescriptions(productDescriptions))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(MyLayout)