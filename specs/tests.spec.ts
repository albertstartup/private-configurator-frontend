import { getNewProductOptionsFromSelections, GlobalOptions, Selections, ProductOptions } from '../src/configurator';
//const jsc = require('jsverify') 

test('configurator', () => {
    const globalOptions: GlobalOptions = {
        colors: {
            'red': {
                id: 'red',
                invalidates: {
                    sizes: ['small']
                }
            },
            'blue': {
                id: 'blue',
                invalidates: {
                    sizes: ['medium']
                }
            }
        },
        quantities: {
            '250': {
                id: '250',
                invalidates: {
                    sizes: ['xlarge']
                }
            }
        }
    }
    const selections: Selections = {
        colors: ['red', 'blue'],
        quantities: ['250']
    }
    const productOptions: ProductOptions = {
        sizes: ['small', 'medium', 'large', 'xlarge'],    
    }
    

    expect(getNewProductOptionsFromSelections(selections,
        productOptions,
        globalOptions)).toEqual({
        sizes: ['large']
    })
})

// JSVerify test for configurator.
// test('configurator', () => {

//     const arbInvalidates = jsc.record({ 'color': jsc.array(jsc.string), 'size': jsc.array(jsc.string) })

//     const result = jsc.assertForall(jsc.string, jsc.string, arbInvalidates, (selectedOptionValue: OptionValueName,
//         selectedOption: OptionName, invalidates: Invalidates) => {
//         const invalidatesWithReason = createInvalidatesWithReasonFromSelectedOptionValue(selectedOptionValue,
//             selectedOption, invalidates)
//         const invalidateWithReason = invalidatesWithReason[selectedOptionValue][0]
//         return invalidateWithReason.reason === selectedOption + ',' + selectedOptionValue
//     })

//     return Promise.resolve(result)
// })