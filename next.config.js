const webpack = require('webpack')
const withTypescript = require("@zeit/next-typescript")
const withCSS = require('@zeit/next-css')

const isProd = process.env.NODE_ENV === 'production'
const isDev = process.env.NODE_ENV === 'development'

module.exports = withTypescript(withCSS({    
    webpack: (config) => {
        
        if (isDev) {
            config.plugins.push(new webpack.DefinePlugin({
                'API_URL': JSON.stringify('http://localhost:6942')
            }))
        }
    
        if (isProd) {            
            if (!process.env.API_URL) { throw Error('No API_URL env var set.') }
            config.plugins.push(new webpack.DefinePlugin({
                'API_URL': JSON.stringify(process.env.API_URL)
            }))
        }

        //Used with semantic-ui to load fonts.
        config.module.rules.push({
            test: /.(png|jpg|gif|svg|eot|ttf|woff|woff2)$/,
            use: {
                loader: 'url-loader',
                options: {
                    limit: 100000
                }
            }
        })
        
        return config
    }
}))