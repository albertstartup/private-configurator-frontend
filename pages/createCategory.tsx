import { FunctionComponent, useState, Dispatch, useEffect } from 'react';
import Layout from '../src/react/lib/Layout';
import { Provider, connect } from 'react-redux';
import store from '../src/redux/store';
import ProductDescriptionFilter from '../src/react/selectorComponents/ProductDescriptionFilter';
import { CustomDivider, createProductCategory, setProductCategoriesInStore } from '../src/react/lib/lib';
import 'antd/dist/antd.css'
import { Input, Button } from 'antd';
import { StateType } from '../src/redux/reducers';
import { AppActions } from '../src/redux/reducers';
import * as actions from '../src/redux/actions'
import { ProductCategory } from '../src/types';
import { ClipLoader } from 'react-spinners'
import * as R from 'ramda'

type ChooseProductPageProps = {
    productCategories: ProductCategory[],
    setProductCategories: (x: ProductCategory[]) => any
}

const CreateCategoryPage: FunctionComponent<ChooseProductPageProps> = (props) => {
    const [value, setValue] = useState('')

    useEffect(() => {
        //setProductCategoriesInStore(productCategories, setProductCategories)
    })

    const onClickCreate = async () => {
        const createdProductCategory = await createProductCategory(value)
        props.setProductCategories(R.concat(props.productCategories, [createdProductCategory]))
    }

    return <Layout headerText='Create New Product Category:'>        
        <ProductDescriptionFilter productTypeDividerText='Choose A Product Type' printTypeDividerText='Choose a Print Type' />

        <CustomDivider text='Name the New Product Category: ' />

        <Input onChange={(e) => setValue(e.target.value)} value={value} style={{ width: '20em' }} size="large" placeholder="Name of The New Product Category" />
        <Button onClick={() => onClickCreate()} style={{ marginLeft: '1em' }} type="primary" size='large'>Create Category</Button>
    </Layout>
}

// Redux Code

const mapStateToProps = (state: StateType) => {
    return {
        productCategories: state.productCategories
    }
}

const mapDispatchToProps = (dispatch: Dispatch<AppActions>) => {
    return {
        setProductCategories: (productCategories: ProductCategory[]) => dispatch(actions.setProductCategories(productCategories))
    }
}

const ConnectedCreateCategoryPage = connect(mapStateToProps, mapDispatchToProps)(CreateCategoryPage)

const ReduxCreateCategoryPage = () => <Provider store={store}>
    <ConnectedCreateCategoryPage />
</Provider>

export default ReduxCreateCategoryPage