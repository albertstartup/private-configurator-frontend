import { AutoComplete, Card, Menu, Button, Input } from 'antd'
import Layout from '../src/react/lib/Layout'
import { FunctionComponent, useState } from 'react';
import { Provider, connect } from 'react-redux';
import store from '../src/redux/store';

const EditOptionPage: FunctionComponent = () => {
    const [value, setValue] = useState('')

    return <Layout headerText='Edit A Product Option'>
        <Input value={value}
            style={{ width: '20em' }}
            size="large" placeholder="Name of The New Option Value" />
        <Button style={{ marginLeft: '1em' }} 
        type="primary" size='large'>Add Option Value</Button>        
    </Layout>
}

// Redux Code

const ConnectedEditOptionPage = connect()(EditOptionPage)

const ReduxEditProductPage = () => <Provider store={store}>
    <ConnectedEditOptionPage />
</Provider>

export default ReduxEditProductPage