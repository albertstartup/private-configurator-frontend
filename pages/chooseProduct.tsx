import Layout from '../src/react/lib/Layout';
import ProductDescriptionFilter from '../src/react/selectorComponents/ProductDescriptionFilter'
import { FunctionComponent, Dispatch, useEffect, useState } from 'react'
import ProductSelector from '../src/react/selectorComponents/ProductSelector';
import store from '../src/redux/store'
import { Provider } from 'react-redux';
import { connect } from 'react-redux';
import { ProductType, ProductDescriptionType, ProductCategory } from '../src/types';
import { StateType } from '../src/redux/reducers';
import { AppActions } from '../src/redux/reducers';
import * as actions from '../src/redux/actions'
import { setProductsAndProductDescriptions, setProductCategoriesInStore } from '../src/react/lib/lib';
import * as R from 'ramda';

type ChooseProductPageProps = {
    products: ProductType[],
    productDescriptions: ProductDescriptionType[],
    productCategories: ProductCategory[],
    setProductCategories: (x: ProductCategory[]) => any
    setProducts: (products: ProductType[]) => any,
    setProductDescriptions: (productDescriptions: ProductDescriptionType[]) => any
}

const ChooseProductPage: FunctionComponent<ChooseProductPageProps> = (props) => {
    const { products, productDescriptions,
        productCategories, setProductCategories,
        setProducts, setProductDescriptions } = props

    useEffect(() => {
        setProductsAndProductDescriptions(products, setProducts, setProductDescriptions)                
    }, [false])

    // Only rerun this if products changes.
    useEffect(() => {
        setProductCategoriesInStore(productCategories, setProductCategories)
    }, [products])

    const [selectedProductType, setSelectedProductType] = useState('')
    const [selectedPrintType, setSelectedPrintType] = useState('')
    const [selectedProductCategory, setSelectedProductCategory] = useState('')
    
    console.log({selectedProductType, selectedPrintType, selectedProductCategory});
    
    return <Layout headerText="Choose A Product To Edit:">

        <ProductDescriptionFilter productCategories={productCategories} onSelectProductCategory={setSelectedProductCategory}
        onSelectProductType={setSelectedProductType}
        onSelectPrintType={setSelectedPrintType} productTypeDividerText='Filter By Product Type:'
            printTypeDividerText='Filter By Print Type:' productCategoryDivideText='Filter By Product Category' />

        <ProductSelector products={products} productDescriptions={productDescriptions}
            filterObj={{productCategory: selectedProductCategory, printType: selectedPrintType, productType: selectedProductType}} dividerText='Choose A Product To Edit' />
    </Layout>
}

// Redux Code

const mapStateToProps = (state: StateType) => {
    return {
        products: state.products,
        productDescriptions: state.productDescriptions,
        productCategories: state.productCategories
    }
}

const mapDispatchToProps = (dispatch: Dispatch<AppActions>) => {
    return {
        setProducts: (products: ProductType[]) => dispatch(actions.setProducts(products)),
        setProductDescriptions: (productDescriptions: ProductDescriptionType[]) => dispatch(actions.setProductDescriptions(productDescriptions)),        
        setProductCategories: (productCategories: ProductCategory[]) => dispatch(actions.setProductCategories(productCategories))
    }
}

const ConnectedChooseProductPage = connect(mapStateToProps, mapDispatchToProps)(ChooseProductPage)

export default () => <Provider store={store}>
    <ConnectedChooseProductPage />
</Provider>