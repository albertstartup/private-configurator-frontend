import { AutoComplete, Card, Menu, Input, Button } from 'antd'
import Layout from '../src/react/lib/Layout'
import { FunctionComponent } from 'react';
import { Provider, connect} from 'react-redux';
import store from '../src/redux/store';
import axios from 'axios'

const createProductOption = async (optionName: string) => {
    try {
        return (await axios.post(`${API_URL}/api/createProductOption/${optionName}`)).data
    } catch (error) {
        throw error
    }    
}

const CreateOptionPage: FunctionComponent = () => {
    return <Layout headerText='Create A New Product Option'>
        <div>Name The New Product Option:</div>
        <Input style={{marginTop: '1em'}} placeholder='What is the new option called?'/>
        <Button type='primary' style={{marginTop: '1em'}}>Create</Button>
    </Layout>
}

// Redux Code

const ConnectedCreateOptionPage = connect()(CreateOptionPage)

const ReduxCreateProductPage = () => <Provider store={store}>
    <ConnectedCreateOptionPage />
</Provider>

export default ReduxCreateProductPage