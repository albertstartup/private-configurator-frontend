import { Card, Row, Col, Button } from 'antd'
import 'antd/dist/antd.css'
import Layout from '../src/react/lib/Layout'
import store from '../src/redux/store'
import { FunctionComponent } from 'react';
import { Provider, connect } from 'react-redux';

const IndexPage: FunctionComponent = () => {
    return <Layout headerText='Home'>
        {/* <h1 style={{ paddingBottom: '1.5em', textAlign: 'center' }}>Home</h1> */}
        <div style={{ padding: '30px' }}>
            <Row gutter={16}>
                <Col style={{ marginRight: '5em' }} span={10}>
                    <Card title="Manage Products:" bordered={true}>
                        <Card style={{ marginBottom: '1em' }} title="Create:" bordered={true}>
                            <Button style={{ marginBottom: '1em' }} type='dashed'>Create A Product</Button>
                            <Button style={{ marginBottom: '1em' }} type='dashed'>Create A Category</Button>
                        </Card>
                        <Card title="Edit:" bordered={true}>
                            <Button style={{ marginBottom: '1em' }} type='dashed'>Edit A Product</Button>
                            <Button style={{ marginBottom: '1em' }} type='dashed'>Edit A Category</Button>
                        </Card>
                    </Card>
                </Col>
                <Col span={10}>
                    <Card title="Manage Options:" bordered={true}>
                        <Card style={{ marginBottom: '1em' }} title="Create:" bordered={true}>
                            <Button style={{ marginBottom: '1em' }} type='dashed'>Create An Option</Button>
                            <Button style={{ marginBottom: '1em' }} type='dashed'>Create An Option Rule</Button>
                        </Card>
                        <Card title="Edit:" bordered={true}>
                            <Button style={{ marginBottom: '1em' }} type='dashed'>Edit An Option</Button>
                            <Button style={{ marginBottom: '1em' }} type='dashed'>Edit An Option Rule</Button>
                        </Card>
                    </Card>
                </Col>
            </Row>
            <Row gutter={16}>
                <Col style={{ marginRight: '5em' }} span={10}>
                    <Card title="Manage Products:" bordered={true}>
                        <Card style={{ marginBottom: '1em' }} title="Create:" bordered={true}>
                            <Button style={{ marginBottom: '1em' }} type='dashed'>Create A Product</Button>
                            <Button style={{ marginBottom: '1em' }} type='dashed'>Create A Category</Button>
                        </Card>
                        <Card title="Edit:" bordered={true}>
                            <Button style={{ marginBottom: '1em' }} type='dashed'>Edit A Product</Button>
                            <Button style={{ marginBottom: '1em' }} type='dashed'>Edit A Category</Button>
                        </Card>
                    </Card>
                </Col>
                <Col span={10}>
                    <Card title="Manage Options:" bordered={true}>
                        <Card style={{ marginBottom: '1em' }} title="Create:" bordered={true}>
                            <Button style={{ marginBottom: '1em' }} type='dashed'>Create An Option</Button>
                            <Button style={{ marginBottom: '1em' }} type='dashed'>Create An Option Rule</Button>
                        </Card>
                        <Card title="Edit:" bordered={true}>
                            <Button style={{ marginBottom: '1em' }} type='dashed'>Edit An Option</Button>
                            <Button style={{ marginBottom: '1em' }} type='dashed'>Edit An Option Rule</Button>
                        </Card>
                    </Card>
                </Col>
            </Row>
        </div>
    </Layout>
}

const ConnectedIndexPage = connect()(IndexPage)

const ReduxIndexPage = () => <Provider store={store}>
    <ConnectedIndexPage />
</Provider>

export default ReduxIndexPage