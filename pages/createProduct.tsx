import CategoryCreator from '../src/react/CategoryCreator'
import { useEffect, FunctionComponent } from 'react';
import { Provider, connect } from 'react-redux';
import store from '../src/redux/store';
import CreateProduct from '../src/react/Product/CreateProduct';
import { ProductCreator } from '../src/react/migration/productCreator';
import { EditProduct } from '../src/react/migration/EditProduct';
import Layout from '../src/react/lib/Layout';
import { Row, Col } from 'antd'

const CreateProductPage: FunctionComponent = () => {
    return <Layout headerText='Create A New Product'>
        <div style={{ textAlign: 'center' }}>
            <Row style={{ marginBottom: '15em' }} gutter={16}>
                <Col span={12}>
                    <div>
                        One
                    </div>
                </Col>
                <Col span={12}>
                    <div>
                        Two
                    </div>
                </Col>
            </Row>
            <Row gutter={16}>
                <Col span={12}>One</Col>
            </Row>
        </div>
    </Layout>
}

const ConnectedCreateProductPage = connect()(CreateProductPage)

const ReduxCreateProductPage = () => <Provider store={store}>
    <ConnectedCreateProductPage />
</Provider>

export default ReduxCreateProductPage